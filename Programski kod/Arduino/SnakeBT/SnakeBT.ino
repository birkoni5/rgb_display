#include <Snake.h>
#include "SPI.h"
#include "Adafruit_WS2801.h"
//#include <IRremote.h>
/*int IR_PIN = 11;
IRrecv irrecv(IR_PIN);
decode_results results;*/
Snake snakeGame(11,8,10);//initialize the snake field with x=15, y=9, delay=10 ticks						 
int score;
uint8_t dataPin  = 2; // MOSI
uint8_t clockPin = 3; // SCK
Adafruit_WS2801 strip = Adafruit_WS2801(88, dataPin, clockPin);
char im[128], data[128];
int barsNumber=11;
int barsHeight=8;
int power=0;
int  led[8][11]={
                {0,1,2,3,4,5,6,7,8,9,10},
                {21,20,19,18,17,16,15,14,13,12,11},
                {22,23,24,25,26,27,28,29,30,31,32},
                {43,42,41,40,39,38,37,36,35,34,33},
                {44,45,46,47,48,49,50,51,52,53,54},
                {65,64,63,62,61,60,59,58,57,56,55},
                {66,67,68,69,70,71,72,73,74,75,76},
                {87,86,85,84,83,82,81,80,79,78,77}
                 };
int digit1_0[12]={19,24,41,46,63,17,18,26,39,48,61,62};
int digit1_1[5]={19,24,41,46,63};
int digit1_2[11]={61,62,63,46,41,40,39,26,17,18,19};
int digit1_3[11]={61,62,63,46,41,40,39,24,17,18,19};
int digit1_4[9]={19,24,41,46,63,40,39,48,61};
int digit1_5[11]={61,62,63,48,41,40,39,24,17,18,19};
int digit1_6[12]={61,62,63,48,41,40,39,24,17,18,19,26};
int digit1_7[7]={19,24,41,46,63,61,62};
int digit1_8[13]={61,62,63,48,41,40,39,24,17,18,19,26,46};
int digit1_9[12]={61,62,63,48,41,40,39,24,17,18,19,46};
int digit2_1[5]={15,28,37,50,59};
int digit2_2[11]={13,14,15,37,50,57,58,59,36,35,30};
int digit2_3[11]={13,14,15,37,50,57,58,59,36,35,28};
int digit2_4[9]={15,28,37,50,59,35,36,52,57};
int digit2_5[11]={13,14,15,37,52,57,58,59,36,35,28};
int digit2_6[12]={13,14,15,37,52,57,58,59,36,35,28,30};
int digit2_7[7]={15,28,37,50,59,57,58};
int digit2_8[13]={13,14,15,28,37,50,57,58,59,52,35,30,36};
int digit2_9[12]={13,14,15,28,37,50,57,58,59,52,35,36};
int r,g,b;
void setup() {
	snakeGame.setBodyColor(0,255,0);//optionally set the color of the snakeparts
	snakeGame.setFoodColor(255,0,0);//optionally set the color of the food
	snakeGame.setHeadColor(255,255,255);//optionally set the color of the snakeparts
        strip.begin();
       // irrecv.enableIRIn(); // Start the receiver
	Serial.begin(9600);
        Serial.write("Snake");
}  
void clearScreen(){
  for (int x=0; x < 11; x++) 
        for (int y=0; y < 8; y++)
          strip.setPixelColor(led[y][x], 0,0,255);
}

void loop() 
{
  


          Snake::pixel* snakeLimbs=snakeGame.getSnakeLimbs();//this needs to be updated every frame 
	Snake::pixel* snakeFood = snakeGame.getFoodPositions();//this needs to be updated every frame	
 

clearScreen();
	strip.setPixelColor(led[snakeFood[0].posY][10-snakeFood[0].posX],snakeFood[0].pixelColor.r,snakeFood[0].pixelColor.g,snakeFood[0].pixelColor.b); // display the food
	for(int i=0; i<snakeGame.getSnakeLenght(); i++)
	{
		//display the snake, my setpixel method has x=0, y=0 at the top left, but the library has it at bottom left, so I invert the Y-Axis:
		strip.setPixelColor(led[snakeLimbs[i].posY][10-snakeLimbs[i].posX],snakeLimbs[i].pixelColor.r,snakeLimbs[i].pixelColor.g,snakeLimbs[i].pixelColor.b);
	}
	strip.show();
        score=snakeGame.getSnakeLenght()-1;
	snakeGame.tick();//main loop for the snake library
	if(snakeGame.wasGameReset())// if the snake bit itself or the wall, flash a little
	{
                
                int digit1=score%10;
                int digit2=(score-digit1)/10;
                
                switch(digit1)
                {
                  case 0:
                     for (int y=0; y < 12; y++)
                     strip.setPixelColor(digit1_0[y], 255,255,255);
                     break;
                  case 1:
                     for (int y=0; y < 5; y++)
                     strip.setPixelColor(digit1_1[y], 255,255,255);
                     break;
                  case 2:
                     for (int y=0; y < 11; y++)
                     strip.setPixelColor(digit1_2[y], 255,255,255);
                     break;
                  case 3:
                     for (int y=0; y < 11; y++)
                     strip.setPixelColor(digit1_3[y], 255,255,255);
                     break;
                  case 4:
                     for (int y=0; y < 9; y++)
                     strip.setPixelColor(digit1_4[y], 255,255,255);
                     break;
                  case 5:
                     for (int y=0; y < 11; y++)
                     strip.setPixelColor(digit1_5[y], 255,255,255);
                     break;
                  case 6:
                     for (int y=0; y < 12; y++)
                     strip.setPixelColor(digit1_6[y], 255,255,255);
                     break;
                  case 7:
                     for (int y=0; y < 7; y++)
                     strip.setPixelColor(digit1_7[y], 255,255,255);
                     break;
                  case 8:
                     for (int y=0; y < 13; y++)
                     strip.setPixelColor(digit1_8[y], 255,255,255);
                     break;
                  case 9:
                     for (int y=0; y < 12; y++)
                     strip.setPixelColor(digit1_9[y], 255,255,255);
                     break;
                }
                switch(digit2)
                {
                  case 1:
                     for (int y=0; y < 5; y++)
                     strip.setPixelColor(digit2_1[y], 255,255,255);
                     break;
                  case 2:
                     for (int y=0; y < 11; y++)
                     strip.setPixelColor(digit2_2[y], 255,255,255);
                     break;
                  case 3:
                     for (int y=0; y < 11; y++)
                     strip.setPixelColor(digit2_3[y], 255,255,255);
                     break;
                  case 4:
                     for (int y=0; y < 9; y++)
                     strip.setPixelColor(digit2_4[y], 255,255,255);
                     break;
                  case 5:
                     for (int y=0; y < 11; y++)
                     strip.setPixelColor(digit2_5[y], 255,255,255);
                     break;
                  case 6:
                     for (int y=0; y < 12; y++)
                     strip.setPixelColor(digit2_6[y], 255,255,255);
                     break;
                  case 7:
                     for (int y=0; y < 7; y++)
                     strip.setPixelColor(digit2_7[y], 255,255,255);
                     break;
                  case 8:
                     for (int y=0; y < 13; y++)
                     strip.setPixelColor(digit2_8[y], 255,255,255);
                     break;
                  case 9:
                     for (int y=0; y < 12; y++)
                     strip.setPixelColor(digit2_9[y], 255,255,255);
                     break;
                }
                strip.show();
                delay(5000);
	}
	else
        delay(30);

}

byte incomingByte=0;
void serialEvent() {
  while (Serial.available()) 
	{
		incomingByte = Serial.read();
		
	}
 /*if(irrecv.decode(&results)) //this checks to see if a code has been received
   {
    if(results.value == 0xFD609F){ // CH+ button
    snakeGame.goUp();

   } 
    //snake will go up on the next move
  if(results.value == 0xFD6897){ // CH- button
    snakeGame.goDown();

  }  //snake will go down on the next move
  if(results.value == 0xFDD827){ // VOL+ button
    snakeGame.goRight();

  }  //snake will go right on the next move
  if(results.value == 0xFD5AA5){ // VOL- button
   snakeGame.goLeft();

  }
irrecv.resume();  
  }*/
	
	switch(incomingByte)
	{
	case 'a':
		snakeGame.goLeft(); //snake will go left on the next move
		break;
	case 'd':
		snakeGame.goRight(); //snake will go right on the next move
		break;
	case 's':
		snakeGame.goDown(); //snake will go down on the next move
		break;
	case 'w':
		snakeGame.goUp(); //snake will go up on the next move
		break;
	case 'on':		
		Serial.write("Snake");
		break;
	//case 'off':		//whaaaaaa
	//	break;
	}
}


	
