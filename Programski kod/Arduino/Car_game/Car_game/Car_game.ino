#include "Adafruit_WS2801.h"
#include "SPI.h"
uint8_t dataPin  = 2; // MOSI
uint8_t clockPin = 3; // SCK
Adafruit_WS2801 strip = Adafruit_WS2801(88, dataPin, clockPin);

int LEFT = 0;
int RIGHT = 1;
#define MAX 8

int idx = 0;
int key = 0;
int score;
int loopCntr = 0;
int road[MAX][2];
int car;
byte incomingByte=0;
byte incomingBytePrev=0;
int  pixel[8][11]={
                {0,1,2,3,4,5,6,7,8,9,10},
                {21,20,19,18,17,16,15,14,13,12,11},
                {22,23,24,25,26,27,28,29,30,31,32},
                {43,42,41,40,39,38,37,36,35,34,33},
                {44,45,46,47,48,49,50,51,52,53,54},
                {65,64,63,62,61,60,59,58,57,56,55},
                {66,67,68,69,70,71,72,73,74,75,76},
                {87,86,85,84,83,82,81,80,79,78,77}
                 };
  int digit1_0[12]={19,24,41,46,63,17,18,26,39,48,61,62};
int digit1_1[5]={19,24,41,46,63};
int digit1_2[11]={61,62,63,46,41,40,39,26,17,18,19};
int digit1_3[11]={61,62,63,46,41,40,39,24,17,18,19};
int digit1_4[9]={19,24,41,46,63,40,39,48,61};
int digit1_5[11]={61,62,63,48,41,40,39,24,17,18,19};
int digit1_6[12]={61,62,63,48,41,40,39,24,17,18,19,26};
int digit1_7[7]={19,24,41,46,63,61,62};
int digit1_8[13]={61,62,63,48,41,40,39,24,17,18,19,26,46};
int digit1_9[12]={61,62,63,48,41,40,39,24,17,18,19,46};
int digit2_1[5]={15,28,37,50,59};
int digit2_2[11]={13,14,15,37,50,57,58,59,36,35,30};
int digit2_3[11]={13,14,15,37,50,57,58,59,36,35,28};
int digit2_4[9]={15,28,37,50,59,35,36,52,57};
int digit2_5[11]={13,14,15,37,52,57,58,59,36,35,28};
int digit2_6[12]={13,14,15,37,52,57,58,59,36,35,28,30};
int digit2_7[7]={15,28,37,50,59,57,58};
int digit2_8[13]={13,14,15,28,37,50,57,58,59,52,35,30,36};
int digit2_9[12]={13,14,15,28,37,50,57,58,59,52,35,36};    
int digit1;
int digit2;
void setup(){
  strip.begin();
  Serial.begin(9600);

  randomSeed(analogRead(5));

  startAgain();
};

void shiftRoad() {
  for (int i=0;i <MAX-1; i++) {
    road[i][LEFT] = road[i+1][LEFT];
    road[i][RIGHT] = road[i+1][RIGHT];
  }
  score++;
}
void moveCar() {
 strip.setPixelColor(pixel[0][car],0,0,0);
strip.show();
  checkKey();
  //reset key value to 'nothing pressed'
  
  if (car <= road[0][LEFT]) {
    car = road[0][LEFT];
    boom();
  }
  if (car >= road[0][RIGHT]) {
    car = road[0][RIGHT];
    boom();
  }
  strip.setPixelColor(pixel[0][car], 0,0,255);
  strip.show();
}

void displayRoad(){
  for(int i=MAX-1; i>-1; i--){
    for (int j=0; j<11; j++) { 
      strip.setPixelColor(pixel[i][j],139,137,137);
    }
    //skip drawing line on edge when initializing
    if (road[i][LEFT] >= 0) {
      
     strip.setPixelColor(pixel[i][road[i][RIGHT]],255,0,0);
     
     strip.setPixelColor(pixel[i][road[i][LEFT]],255,0,0);
    }
    for(int x=0;x<road[i][LEFT];x++)
     strip.setPixelColor(pixel[i][x],255,0,0);
    for(int x=road[i][RIGHT];x<11;x++)
     strip.setPixelColor(pixel[i][x],255,0,0);
}

strip.show();
}

void checkKey() {
 /* int static prev = 1023;
  
  int r = analogRead(0);
  
  if (r < 20 && prev > 20) {
    key = 1;
  } else if (r >= 20 && r < 530 && (prev < 20 || prev > 530 )) {
    key = -1;
  }
  
  prev = r;*/
  while (Serial.available()) 
	{
		incomingByte = Serial.read();
		Serial.write(incomingByte);
	}
if (incomingByte/=incomingBytePrev)
	switch(incomingByte)
	{
	case 'a':
		car++;
		break;
	case 'd':
		car--;
		break;
}
else
 car=car;
incomingBytePrev=incomingByte;

}

void createRoad() {
  int r = random(0,3);
  switch(r) {
    case 0:
      road[MAX-1][LEFT] = road[MAX-2][LEFT]-1;
      break;
    case 1:
      road[MAX-1][LEFT] = road[MAX-2][LEFT]+1;
      break;
    case 2:
      break;
  };

  if (road[MAX-1][LEFT] < 0 )
    road[MAX-1][LEFT] = 0;
    
  if (road[MAX-1][LEFT] > 5 )
    road[MAX-1][LEFT] = 5;
    
  road[MAX-1][RIGHT] =  road[MAX-1][LEFT] + 5;
  
  Serial.print(road[MAX-1][LEFT]);
  Serial.print(", ");
  Serial.println(road[0][RIGHT]);
}

void startAgain() {
  strip.show();
  for (int i=0;i<= MAX; i++) {
    for (int j=0; j<11; j++) {
      strip.setPixelColor(pixel[i][j],0);
    };
    road[i][LEFT] = -1;
    road[i][RIGHT] = -1;
  };
  delay(1000);
  
  road[MAX-1][LEFT] = random(0,5);
  road[MAX-1][RIGHT] = road[0][LEFT]+5;
  for(int i=0;i< MAX-1; i++) {
   createRoad();
   shiftRoad();
   displayRoad();
   delay(i*20);
  }
  car = road[0][LEFT]+2;
  key = 0;
  moveCar();
  delay(1000);

};

void boom() {
// LedSign::Set(MAX-1, car, 0);
strip.setPixelColor(pixel[1][car-1],255,122,0);
strip.setPixelColor(pixel[1][car+1],255,122,0);
strip.setPixelColor(pixel[0][car+1],255,122,0);
strip.setPixelColor(pixel[0][car-1],255,122,0);
strip.show();
 delay(300);
strip.setPixelColor(pixel[2][car-2],255,122,0);
strip.setPixelColor(pixel[2][car+2],255,122,0);
strip.setPixelColor(pixel[0][car+2],255,122,0);
strip.setPixelColor(pixel[0][car-2],255,122,0);
strip.show();
 delay(500);
strip.setPixelColor(pixel[3][car-3],255,122,0);
strip.setPixelColor(pixel[3][car+3],255,122,0);
strip.show();
 delay(1500);
  score=(score-score%10)/10;
  digit1=score%10;
  digit2=(score-digit1)/10;
                
                switch(digit1)
                {
                  case 0:
                     for (int y=0; y < 12; y++)
                     strip.setPixelColor(digit1_0[y], 0,255,0);
                     break;
                  case 1:
                     for (int y=0; y < 5; y++)
                     strip.setPixelColor(digit1_1[y], 0,255,0);
                     break;
                  case 2:
                     for (int y=0; y < 11; y++)
                     strip.setPixelColor(digit1_2[y], 0,255,0);
                     break;
                  case 3:
                     for (int y=0; y < 11; y++)
                     strip.setPixelColor(digit1_3[y], 0,255,0);
                     break;
                  case 4:
                     for (int y=0; y < 9; y++)
                     strip.setPixelColor(digit1_4[y], 0,255,0);
                     break;
                  case 5:
                     for (int y=0; y < 11; y++)
                     strip.setPixelColor(digit1_5[y], 0,255,0);
                     break;
                  case 6:
                     for (int y=0; y < 12; y++)
                     strip.setPixelColor(digit1_6[y], 0,255,0);
                     break;
                  case 7:
                     for (int y=0; y < 7; y++)
                     strip.setPixelColor(digit1_7[y], 0,255,0);
                     break;
                  case 8:
                     for (int y=0; y < 13; y++)
                     strip.setPixelColor(digit1_8[y], 0,255,0);
                     break;
                  case 9:
                     for (int y=0; y < 12; y++)
                     strip.setPixelColor(digit1_9[y], 0,255,0);
                     break;
                }
                switch(digit2)
                {
                  case 1:
                     for (int y=0; y < 5; y++)
                     strip.setPixelColor(digit2_1[y], 0,255,0);
                     break;
                  case 2:
                     for (int y=0; y < 11; y++)
                     strip.setPixelColor(digit2_2[y], 0,255,0);
                     break;
                  case 3:
                     for (int y=0; y < 11; y++)
                     strip.setPixelColor(digit2_3[y], 0,255,0);
                     break;
                  case 4:
                     for (int y=0; y < 9; y++)
                     strip.setPixelColor(digit2_4[y], 0,255,0);
                     break;
                  case 5:
                     for (int y=0; y < 11; y++)
                     strip.setPixelColor(digit2_5[y], 0,255,0);
                     break;
                  case 6:
                     for (int y=0; y < 12; y++)
                     strip.setPixelColor(digit2_6[y], 0,255,0);
                     break;
                  case 7:
                     for (int y=0; y < 7; y++)
                     strip.setPixelColor(digit2_7[y], 0,255,0);
                     break;
                  case 8:
                     for (int y=0; y < 13; y++)
                     strip.setPixelColor(digit2_8[y], 0,255,0);
                     break;
                  case 9:
                     for (int y=0; y < 12; y++)
                     strip.setPixelColor(digit2_9[y], 0,255,0);
                     break;
                }
                strip.show();
                delay(5000);
	score=0;
 startAgain();
 
}

void loop(){
  if (loopCntr++ == 20) {
   shiftRoad();
   createRoad();
   displayRoad();
   moveCar();
   loopCntr = 0;
  } 
    
  
  delay(10);
};

